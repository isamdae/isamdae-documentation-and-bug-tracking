//
// MEL translation of the battery cell model with faults from
// Krysander, Hashimnya and Frisk.
//
// (c) M. Malandain <mathias.malandain@inria.fr>,
// Inria centre at Rennes University,
// 2019-2022
//
// Part of IsamDAE: a structural analysis library for multimode DAE system,
// inspired by J. Pryce's Sigma method.
//

// Constants

constant Rp : real;
constant Cp : real;
constant Em : real;
constant R0 : real;

// Measurements (handled as constants for the structural analysis)

constant y_i : real;
constant y_v : real;

// Mode variables

s1 : boolean; // true when S1 is closed
s2 : boolean; // true when S2 is closed
s3 : boolean; // true when S3 is closed
s4 : boolean; // true when S4 is closed

// There is always exactly one switch closed on each side
// of the battery cell (the submodule is always connected
// to the pack, no shorting) -> only 4 modes taken into account

invariant (s1 ^ s2) & (s3 ^ s4); // ^ = xor

// Faults (declared as constants for now)

constant f_cell : real;
constant f_i : real;
constant f_v : real;

// Equations of component 1

e1 : v_p_der = i_cell / Cp - v_p / (Rp * Cp);
e2 : v_cell = v_p + R0 * i_cell + Em + f_cell;
e3 : v_p_der = der(v_p);
e4 : v_sm =
       if (s1 & s4) then v_cell else
       if (s2 & s3) then - v_cell
       else 0.;
e5 : i_cell =
       if (s1 & s4) then i_pack else
       if (s2 & s3) then - i_pack
       else 0.;
e6 : y_i = i_cell + f_i;
e7 : y_v = v_cell + f_v;