//
// Small circuit with two switches; structurally singular when
// both switches are open.
//
// Part of IsamDAE
//
// (c) Benoit Caillaud & Mathias Malandain, Inria Rennes, France, 2019
//

constant U0 : real = 10.;
constant R : real = 10.;
constant L : real = 1.;

u1 : real;
u2 : real;
v : real;
w : real;
j : real;

p : boolean;
q : boolean;

kirchhoff : equation U0 + u1 + u2 + v + w = 0;
resistor : equation R * j + v = 0;
inductance : equation w + L * der(j) = 0;
switch_p : equation 0 = if p then u1 else j;
switch_q : equation 0 = if q then u2 else j;

// invariant p|q;
